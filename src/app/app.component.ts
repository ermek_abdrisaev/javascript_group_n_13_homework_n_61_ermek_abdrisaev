import { Component } from '@angular/core';
import { News } from './shared/new.model';
import { PublicationItem } from './shared/publication.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  location = 'home';

  onNavigate(where: string){
    this.location = where;
  }
}
