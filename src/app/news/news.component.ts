import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { News } from '../shared/new.model';
import { NewsService } from '../shared/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
})
export class NewsComponent implements OnInit{
  newses!: News[];

  constructor(private newsService: NewsService){}

  ngOnInit(){
    this.newses = this.newsService.getNewses();
    this.newsService.newsesChange.subscribe((newses: News[]) =>{
      this.newses = newses;
    });
  }

}
