import { Component, EventEmitter, Input, Output } from '@angular/core';
import { News } from '../../shared/new.model';
import { PublicationService } from '../../shared/publication.service';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.css']
})
export class NewsItemComponent {
  @Input() news!: News;

  constructor(private pubblicationService: PublicationService){}

  onClick(){
    this.pubblicationService.addNewsToPublications(this.news);
  }
}
