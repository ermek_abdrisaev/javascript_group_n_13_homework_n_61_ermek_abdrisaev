import { News } from './new.model';

export class PublicationItem {
  constructor(
    public news: News,
    public amount: number = 1,
  ){}

  getPrice(){
    return this.news.price * this.amount;
  }
}
