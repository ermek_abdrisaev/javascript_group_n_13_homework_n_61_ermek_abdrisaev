export class News {
  constructor(
    public header: string,
    public image: string,
    public text: string,
    public author: string,
    public price: number,
  ){}

}
