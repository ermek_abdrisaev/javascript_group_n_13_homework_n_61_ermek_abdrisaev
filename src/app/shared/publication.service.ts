import { EventEmitter } from '@angular/core';
import { PublicationItem } from './publication.model';
import { News } from './new.model';

export class PublicationService {
  publicationItemsChange = new EventEmitter<PublicationItem[]>();
  publicationItems: PublicationItem[] = [];

  getItems(){
    return this.publicationItems.slice();
  }

  addNewsToPublications(news: News){
    const existinItem = this.publicationItems.find(publicationItem => publicationItem.news === news);

    if(existinItem){
      existinItem.amount++;
    }else {
      const newItem = new PublicationItem(news);
      this.publicationItems.push(newItem);
    }

    this.publicationItemsChange.emit(this.publicationItems);

  }

}
