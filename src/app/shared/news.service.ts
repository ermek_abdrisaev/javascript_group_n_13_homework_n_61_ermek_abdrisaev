import { News } from './new.model';
import { EventEmitter } from '@angular/core';

  export class NewsService {
    newsesChange = new EventEmitter<News[]>();

    private newses: News[] = [
      new News('New year', 'https://pbs.twimg.com/media/FFlSBYEXsAQcsMH?format=jpg', 'New year 2021 comming', 'Garry Linden', 20),
      new News('What we going to do at new year', 'https://pbs.twimg.com/media/FFlSBYEXsAQcsMH?format=jpg', 'Everybody in world wait New year, especially after COVID-19 and lockdowns', 'Luis Cruz', 20),
    ];
    getNewses(){
      return this.newses.slice();
    }

    addNews(news: News){
      this.newses.push(news);
      this.newsesChange.emit(this.newses);
    }
}
