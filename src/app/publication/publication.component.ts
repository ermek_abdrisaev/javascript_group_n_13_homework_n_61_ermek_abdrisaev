import { Component, Input, OnInit } from '@angular/core';
import { PublicationItem } from '../shared/publication.model';
import { PublicationService } from '../shared/publication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
  publicationItems!: PublicationItem[];
  modalOpen = false;

  constructor(
    public publicationService: PublicationService,
    public router: Router,
  ) {}

  ngOnInit(): void {
    this.publicationItems = this.publicationService.getItems();
    this.publicationService.publicationItemsChange.subscribe((publicationItems: PublicationItem[]) => {
      this.publicationItems = publicationItems;
    })
  }

  getTotalPrice(){
    return this.publicationItems.reduce((sum, publicationItem)=>{
      return sum + publicationItem.getPrice();
    }, 0);
  }

  openCheckoutModal(){
    this.modalOpen = true;
  }

  closeCheckoutModal(){
    this.modalOpen = false;
  }

  continue(){
    this.router.navigate(['/join'])
  }
}
