import { Component, EventEmitter, Input, Output } from '@angular/core';
import { News } from '../../shared/new.model';
import { PublicationItem } from '../../shared/publication.model';

@Component({
  selector: 'app-publication-item',
  templateUrl: './publication-item.component.html',
  styleUrls: ['./publication-item.component.css']
})
export class PublicationItemComponent {
  @Input() publicationItem!: PublicationItem;

}
