import { Component, ElementRef,  ViewChild } from '@angular/core';
import { News } from '../shared/new.model';
import { NewsService } from '../shared/news.service';

@Component({
  selector: 'app-new-news',
  templateUrl: './new-news.component.html',
  styleUrls: ['./new-news.component.css'],
})
export class NewNewsComponent {
  @ViewChild('headerInput') headerInput!: ElementRef;
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('textInput') textInput!: ElementRef;
  @ViewChild('authorInput') authorInput!: ElementRef;
  @ViewChild('priceInput') priceInput!: ElementRef;

  constructor(public newsService: NewsService){}

  createNews(){
    const header: string = this.headerInput.nativeElement.value;
    const image: string = this.imageInput.nativeElement.value;
    const text: string = this.textInput.nativeElement.value;
    const author: string = this.authorInput.nativeElement.value;
    const price = parseFloat(this.priceInput.nativeElement.value);

    const news = new News(header, image, text, author, price);
    this.newsService.addNews(news);
  }

}
