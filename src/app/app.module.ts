import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewNewsComponent } from './new-news/new-news.component';
import { NewsComponent } from './news/news.component';
import { NewsItemComponent } from './news/news-item/news-item.component';
import { PublicationComponent } from './publication/publication.component';
import { PublicationItemComponent } from './publication/publication-item/publication-item.component';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { NewsService } from './shared/news.service';
import { PublicationService } from './shared/publication.service';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { ModalComponent } from './ui/modal/modal.component';
import { JoinComponent } from './join.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new-news', component: NewNewsComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', component: JoinComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewNewsComponent,
    NewsComponent,
    NewsItemComponent,
    PublicationComponent,
    PublicationItemComponent,
    HomeComponent,
    ContactsComponent,
    ModalComponent,
    JoinComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [NewsService, PublicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
